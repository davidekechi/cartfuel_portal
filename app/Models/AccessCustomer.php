<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AccessCustomer extends Model
{
    protected $guarded = [];

    use HasFactory;
}
