<?php

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;

use App\Models\User;

function valueExists($array, $key, $val) {
    foreach ($array as $item){
        if (isset($item[$key]) && $item[$key] == $val){
            return $item;
        }
    }
    return 'false';
}