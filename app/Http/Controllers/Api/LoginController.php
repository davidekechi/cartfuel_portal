<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\User;

class LoginController extends Controller
{
    public function index(Request $request){
        $email = $request->email;
        if(User::where('email', $email)->count() > 0){
            return 'Login Successful';
        }else{
            return abort(401);
        }
    }
}
