<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use Illuminate\Support\Str;

use App\Http\Requests;

use App\Models\User;

use App\Models\StripeToken;

use App\Models\AccessCustomer;

class StripeController extends Controller
{
    public function connect(Request $request){
        $stripe = new \Stripe\StripeClient('sk_test_51H0c3hHEOFw43cPOSQEpL5EN6wI1fOzJJN9rkDNVzfDPAkAMt3hytNsQo8Q8bYFCqME8v5Hwhzh38lIa1z9d8g0I008whK4ceQ');
        //$stripe = new \Stripe\StripeClient('sk_test_51Ke7vpE7FSal75hBnvcLPRBeA15NeKmu9kKycBfRCbbDH9PhdxaaxkbEaKQT425L0oAw70A0KGaClhUYadLCTtSJ00zgstsNAO');

        $token = Str::random();

        StripeToken::where('email',$request->email)->delete();

        StripeToken::create([
            'email' => $request->email,
            'token' => $token,
        ]);
        
        // Create account
        $account = $stripe->accounts->create([
            'country' => 'GB',
            'type'    => 'standard',
            'email'   => $request->email,
        ]);

        User::where('email', $request->email)->update([
            'stripe_id' => $account->id,
        ]);

        if(User::where('email', $request->email)->where('stripe_id', $account->id)->count() < 1){
            abort('500');
        }

        $onboardLink = $stripe->accountLinks->create([
            'account'     => $account->id,
            'refresh_url' => 'https://portal.cartfuel.io/connect-stripe',
            'return_url'  => 'https://portal.cartfuel.io/connect-stripe?status=true&token='.$token,
            'type'        => 'account_onboarding'
        ]);

        return $output = [
            'url' => $onboardLink->url,
        ];
    }

    public function onboard(Request $request){
        $stripe = new \Stripe\StripeClient('sk_test_51H0c3hHEOFw43cPOSQEpL5EN6wI1fOzJJN9rkDNVzfDPAkAMt3hytNsQo8Q8bYFCqME8v5Hwhzh38lIa1z9d8g0I008whK4ceQ');
        //$stripe = new \Stripe\StripeClient('sk_test_51Ke7vpE7FSal75hBnvcLPRBeA15NeKmu9kKycBfRCbbDH9PhdxaaxkbEaKQT425L0oAw70A0KGaClhUYadLCTtSJ00zgstsNAO');

        if(StripeToken::where('token', $request->token)->count() < 1){
            abort('401');
        }

        $token_email = StripeToken::where('token', $request->token)->value('email');

        if(User::where('email', $token_email)->value('stripe_id') == 'Null'){
            abort('401');
        }

        $stripe_id = User::where('email', $token_email)->value('stripe_id');

        $accounts = $stripe->accounts->retrieve(
            $stripe_id,
        );

        $details = $accounts->details_submitted ? 'true' : 'false';

        if ($details == 'true') {
            User::where('email', $token_email)->update([
                'onboarding' => 'true',
            ]);
        }

        return $output = [
            'details' => $details,
        ];
    }

    public function customers($stripe_id){
        $stripe = new \Stripe\StripeClient('sk_test_51H0c3hHEOFw43cPOSQEpL5EN6wI1fOzJJN9rkDNVzfDPAkAMt3hytNsQo8Q8bYFCqME8v5Hwhzh38lIa1z9d8g0I008whK4ceQ');
        //$stripe = new \Stripe\StripeClient('sk_test_51Ke7vpE7FSal75hBnvcLPRBeA15NeKmu9kKycBfRCbbDH9PhdxaaxkbEaKQT425L0oAw70A0KGaClhUYadLCTtSJ00zgstsNAO');

        $raw_customers = $stripe->customers->all(
            ['limit' => 100],
            ['stripe_account' => $stripe_id]
        )->data;

        $customers = [];

        foreach ($raw_customers as $key => $value) {

            $customers[] = [
                'id' => $value->id,
                'name' => $value->name,
                'email' => $value->email
            ];
        }
        
        return $customers;
    }

    public function access(Request $request){
        $stripe = new \Stripe\StripeClient('sk_test_51H0c3hHEOFw43cPOSQEpL5EN6wI1fOzJJN9rkDNVzfDPAkAMt3hytNsQo8Q8bYFCqME8v5Hwhzh38lIa1z9d8g0I008whK4ceQ');
        //$stripe = new \Stripe\StripeClient('sk_test_51Ke7vpE7FSal75hBnvcLPRBeA15NeKmu9kKycBfRCbbDH9PhdxaaxkbEaKQT425L0oAw70A0KGaClhUYadLCTtSJ00zgstsNAO');

        $stripe_id = $request->stripe_id;

        $token = Str::random();

        $raw_customers = $stripe->customers->all(
            ['limit' => 1000000],
            ['stripe_account' => $stripe_id]
        )->data;

        $customers = [];

        foreach ($raw_customers as $key => $value) {

            $customers[] = [
                'id' => $value->id,
                'name' => $value->name,
                'email' => $value->email
            ];
        }
        

        $key = valueExists($customers, 'email', $request->email);

        if ($key == 'false') {
            abort('401');
        }else{
            AccessCustomer::where('email', $request->email)->delete();

            AccessCustomer::create([
                'email' => $request->email,
                'customer_id' => $key['id'],
                'stripe_id' => $stripe_id,
                'token' => $token,
            ]);

            $output = [
                'email' => $request->email,
                'token' => $token
            ];

            return $output;
        }
    }

    public function billing($stripe_id, $customer_id){
        $stripe = new \Stripe\StripeClient('sk_test_51H0c3hHEOFw43cPOSQEpL5EN6wI1fOzJJN9rkDNVzfDPAkAMt3hytNsQo8Q8bYFCqME8v5Hwhzh38lIa1z9d8g0I008whK4ceQ');
        //$stripe = new \Stripe\StripeClient('sk_test_51Ke7vpE7FSal75hBnvcLPRBeA15NeKmu9kKycBfRCbbDH9PhdxaaxkbEaKQT425L0oAw70A0KGaClhUYadLCTtSJ00zgstsNAO');


        $session = $stripe->billingPortal->sessions->create([
            'customer' => $customer_id,
            'return_url' => 'https://portal.cartfuel.io/dashboard',
        ], ['stripe_account' => $stripe_id]);
        
        return $output = [
            'url' => $session->url,
        ];
    }

    public function customer_billing($stripe_id, $customer_id, $token, $slug){
        $stripe = new \Stripe\StripeClient('sk_test_51H0c3hHEOFw43cPOSQEpL5EN6wI1fOzJJN9rkDNVzfDPAkAMt3hytNsQo8Q8bYFCqME8v5Hwhzh38lIa1z9d8g0I008whK4ceQ');
        //$stripe = new \Stripe\StripeClient('sk_test_51Ke7vpE7FSal75hBnvcLPRBeA15NeKmu9kKycBfRCbbDH9PhdxaaxkbEaKQT425L0oAw70A0KGaClhUYadLCTtSJ00zgstsNAO');

        AccessCustomer::where('token', $token)->delete();

        $session = $stripe->billingPortal->sessions->create([
            'customer' => $customer_id,
            'return_url' => 'https://portal.cartfuel.io/access/'.$slug,
        ], ['stripe_account' => $stripe_id]);
        
        return $output = [
            'url' => $session->url,
        ];
    }
}
