<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\User;

use App\Models\AccessCustomer;

use App\Http\Resources\AccessCustomerResource;

class AccessCustomerController extends Controller
{
    public function index($email, $token){
        if(AccessCustomer::where('email', $email)->where('token', $token)->count() > 0){
            $company_stripe_id = AccessCustomer::where('email', $email)->where('token', $token)->value('stripe_id');
            $company_account = User::where('stripe_id', $company_stripe_id)->value('account');
            $access_customer = AccessCustomer::where('email', $email)->where('token', $token)->get();
            $output = [
                'access_customer' => $access_customer,
                'company_account' => $company_account
            ];
            
            return $output;
        }else{
            return abort(401);
        }
    }
}
