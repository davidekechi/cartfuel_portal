<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\User;

use App\Http\Resources\UserResource;

class UserController extends Controller
{
    public function index($email){
        if(User::where('email', $email)->count() > 0){
            return UserResource::collection(User::get());
        }else{
            return abort(401);
        }
    }

    public function company($company){
        if(User::where('slug', $company)->count() > 0){
            return UserResource::collection(User::get());
        }else{
            return abort(404);
        }
    }
}
