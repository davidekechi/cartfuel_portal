<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\User;

use App\Models\RegisterMagic;

class RegisterController extends Controller
{
    

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function create(Request $request){
        $request->validate([
            'name' => 'required',
            'email' => 'required|email',
            'account' => 'required'
        ]);

        $domain_account = strtolower(str_replace(" ", "_", $request->account));

        $domain = 'https://portal.cartfuel.io/access/'.$domain_account;

        RegisterMagic::where('email', $request->email)->delete();

        User::create([
            'name' => $request->name,
            'email' => $request->email,
            'account' => $request->account,
            'slug' => $domain_account,
            'domain' => $domain,
        ]);

        return 'Registration Successful';
    }
}
