<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models\User;

use App\Models\RegisterMagic;

use App\Http\Resources\RegisterMagicResource;


class RegisterMagicController extends Controller
{
    public function index($email){
        if(RegisterMagic::where('email', $email)->count() > 0){
            return RegisterMagicResource::collection(RegisterMagic::where('email', $email)->get());
        }else{
            return abort(401);
        }
    }

    public function store(Request $request){
        $request->validate([
            'name' => 'required',
            'email' => 'required|email',
            'account' => 'required'
        ]);

        if(User::where('email', $request->email)->count() < 1){

            RegisterMagic::where('email', $request->email)->delete();

            RegisterMagic::create([
                'name' => $request->name,
                'email' => $request->email,
                'account' => $request->account
            ]);

            return response()->json([
                'msg' => 'Data Received',
            ]);
        }else{
            return response()->json([
                'msg' => 'An account with this email address already exists!',
            ], 401);
        }
    }
}