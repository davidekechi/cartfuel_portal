<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('get-user/{email}', '\App\Http\Controllers\Api\UserController@index');

Route::get('get-company/{email}', '\App\Http\Controllers\Api\UserController@company');

Route::get('get-register-magic/{email}', '\App\Http\Controllers\Api\RegisterMagicController@index');

Route::get('get-access-user/{email}/{token}', '\App\Http\Controllers\Api\AccessCustomerController@index');

Route::post('register-magic', '\App\Http\Controllers\Api\RegisterMagicController@store');

Route::post('register-user', '\App\Http\Controllers\Api\RegisterController@create');

Route::post('login-user', '\App\Http\Controllers\Api\LoginController@index');

Route::post('login-access', '\App\Http\Controllers\Api\StripeController@access');

Route::post('connect-stripe', '\App\Http\Controllers\Api\StripeController@connect');

Route::put('set-onboard', '\App\Http\Controllers\Api\StripeController@onboard');

Route::get('get-customers/{stripe_id}', '\App\Http\Controllers\Api\StripeController@customers');

Route::get('get-billing/{stripe_id}/{customer_id}', '\App\Http\Controllers\Api\StripeController@billing');

Route::get('get-customer-billing/{stripe_id}/{customer_id}/{token}/{slug}', '\App\Http\Controllers\Api\StripeController@customer_billing');
