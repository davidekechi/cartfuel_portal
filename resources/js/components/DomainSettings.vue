<template>
  <div>
    <div class="section-in-base middle wf-section" v-show="loadingPlaceholder">
        <div class="div-block-6">
            <div>
                <div class="div-block-61">
                    <div v-show="stillLoading">
                        <svg height="100" width="100" style="margin-left: 0%; margin-top:50px;">
                            <circle
                                stroke-linecap="round"
                                transform-origin="50 50"
                                cx="50"
                                cy="50"
                                r="40"
                                stroke-width="7"
                                stroke="#525965"
                                fill="none"
                                stroke-dasharray="251.32"
                                stroke-dashoffset="251.32"
                            >
                                <animate
                                attributeName="stroke-dashoffset"
                                dur="4s"
                                to="-251.32"
                                repeatCount="indefinite"
                                />
                                <animateTransform
                                attributeName="transform"
                                type="rotate"
                                dur="3.5s"
                                to="360"
                                repeatCount="indefinite"
                                />
                            </circle>
                        </svg>
                        <br/><br/><br/>
                        <div class="text-block-10 register magic">Please wait...</div>
                    </div>
                    <div class="w-form-fail" v-show="error_display">
                        <div>Oops! We encountered an error verifying your account. Please <router-link to="/register">continue</router-link> to try again</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="main-app-wrapper wf-section" v-show="loadedSection">
      <div class="container-1280">
        <div class="dashboardwrapper">
          <div class="dashboardgrid settings">
            <div id="w-node-_0c60c811-0e3f-fd96-d24e-7bbf7c8c00f9-297e46b5" class="rightdashboarddiv">
              <div class="settingswrapper">
                <div class="settingsdiv">
                  <div class="settingscategorycard">
                    <router-link to="/dashboard" class="apptext settings">Dashboard</router-link>
                  </div>
                  <div class="settingscategorycard">
                    <router-link to="/portal-domain-settings.html" aria-current="page" class="apptext settings w--current">Domain Settings</router-link>
                  </div>
                </div>
              </div>
            </div>
            <div class="rightdashboarddiv">
              <div class="getreadycard">
                <div>
                  <div class="div-block-23">
                    <div class="text-block-4 bold big margin-bottom-2">Add Your Own Domain</div>
                  </div>
                  <div class="apptext grey smaller margin-bottom-2">Connect your custom domain by creating a CNAME record with the below values in your domains DNS settings<a href="#"><br></a>
                  </div>
                  <div class="div-block-21 margin-top-4 _1"><img src="images/TypeAction-Iconsorders.svg" loading="lazy" alt="" class="image-8">
                    <a href="https://docs.cartfuel.io/en/articles/5781098-how-do-i-connect-my-stripe-account" class="apptext documentation">View Documentation</a>
                  </div>
                  <div class="infodiv margin-top-12">
                    <p class="apptext info"><strong>TYPE:</strong> CNAME | <strong>HOST:</strong> your.domain.example.com | <strong>ANSWER:</strong> app.cartfuel.io</p>
                  </div>
                </div>
                <div class="smtpsettingswrapper margin-top-8">
                  <div>
                    <div class="w-form">
                      <form id="email-form" name="email-form" data-name="Email Form" method="get"><label for="name" class="apptext">Custom Domain</label><input type="text" class="form-input w-input" maxlength="256" name="name" data-name="Name" placeholder="app.cartfuel.io" id="name"><label for="email" class="apptext">Root URL Redirection </label><input type="text" class="form-input w-input" maxlength="256" name="email" data-name="Email" placeholder="https://example.com/domain/page" id="email" required=""><input type="submit" value="Save" data-wait="Please wait..." class="button button-small margin-top-8 w-button"></form>
                      <div class="w-form-done" v-show="success_display">
                        <div>Thank you! Your submission has been received!</div>
                      </div>
                      <div class="w-form-fail" v-show="error_display">
                        <div>Oops! Something went wrong while submitting the form.</div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</template>

<script>

export default{
    name: 'DomainSettings',
    data(){
        return {
            loadingPlaceholder: true,
            stillLoading: true,
            loadedSection: false,
            customerPlaceholder: true,
            customerLoading: true,
            customerLoaded: false,
            empty_display: false,
            success_display: false,
            error_display: false,
            customer_error_display: false,
            user: {
                name: '',
                email: '',
                account: '',
                domain: '',
                created_at: ''
            },
            customers: {},
        }
    },
    methods: {
        customer_e(){
            this.customerLoading = false,
            this.customer_error_display = true;
        },

        async render() {
          const magic = this.magic_user;

          const isLoggedIn = await magic.user.isLoggedIn();

          if (isLoggedIn) {
              /* Get user metadata including email */
              const userMetadata = await magic.user.getMetadata();
              this.getUser(userMetadata.email);
          }else{
              /*
              For this tutorial, our callback route is simply "/callback"
              */
              try {
              /* Complete the "authentication callback" */
              await magic.auth.loginWithCredential();

              /* Get user metadata including email */
              const userMetadata = await magic.user.getMetadata();
              this.getUser(userMetadata.email);
              } catch {
              /* In the event of an error, we'll go back to the login page */

              this.$router.push('/login');
              }
          }
        },

        async getUser(email){
            const res = await fetch(`/api/get-user/${email}`);

            if(res.status === 200){
                const data = await res.json();

                if(data.data[0].onboarding == 'false'){
                    this.$router.push('/connect-stripe');
                }else{
                  this.loadingPlaceholder = false;
                  this.loadedSection = true;
                  this.user.account = data.data[0].account;
                  this.user.name = data.data[0].name.split(" ")[0];
                  this.user.email = email;
                  this.user.stripe_id = data.data[0].stripe_id;
                  this.user.domain = data.data[0].domain;
                  this.user.created_at = data.data[0].created_at;
                  this.intercom();
                }
            }else{
                this.handleLogout();
            }
        },

        async handleLogout(){
          const magic = this.magic_user;

          await magic.user.logout();

          this.$router.push('/login');
        },

        intercom(){
          window.intercomSettings = {
              api_base: "https://api-iam.intercom.io",
              app_id: "kj6j3xrr",
              name: this.user.name,
              email: this.user.email,
              created_at: this.user.created_at
          };

          // We pre-filled your app ID in the widget URL: 'https://widget.intercom.io/widget/kj6j3xrr'
          (function(){var w=window;var ic=w.Intercom;if(typeof ic==="function"){ic('reattach_activator');ic('update',w.intercomSettings);}else{var d=document;var i=function(){i.c(arguments);};i.q=[];i.c=function(args){i.q.push(args);};w.Intercom=i;var l=function(){var s=d.createElement('script');s.type='text/javascript';s.async=true;s.src='https://widget.intercom.io/widget/kj6j3xrr';var x=d.getElementsByTagName('script')[0];x.parentNode.insertBefore(s,x);};if(document.readyState==='complete'){l();}else if(w.attachEvent){w.attachEvent('onload',l);}else{w.addEventListener('load',l,false);}}})();
        }
    },
    created() {
        this.render();
    }
}

</script>