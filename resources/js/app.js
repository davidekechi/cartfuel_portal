import { createApp } from 'vue';

import { Magic } from 'magic-sdk';

require('./bootstrap');

import Router from './router.js';
import App from './components/App.vue';

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */


const app = createApp({});

app.config.globalProperties.magic_user = new Magic('pk_live_DFB299AF23842FA3');

app.config.globalProperties.magic_customer = new Magic('pk_live_AD0017791F50EC24');

app.component('app', App);

app.use(Router).mount('#app');

//createApp(App)
//.use(Router)
//.mount('#app');