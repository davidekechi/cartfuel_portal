import {createRouter, createWebHistory} from 'vue-router'

import Dashboard from './components/Dashboard';
import DomainSettings from './components/DomainSettings';
import ConnectStripe from './components/ConnectStripe';
import ConfigureStripe from './components/ConfigureStripe';
import Login from './components/Login';
import Register from './components/Register';
import ConfirmLogin from './components/ConfirmLogin';
import CheckEmail from './components/CheckEmail';
import ConfirmAccount from './components/ConfirmAccount';
import AccessPage from './components/AccessPage';
import CustomerCheckEmail from './components/CustomerCheckEmail';
import AccessDashboard from './components/AccessDashboard';
import LaunchPortal from './components/LaunchPortal';
import PageNotFound from './components/PageNotFound';

const routes = [
    {
        path: '/dashboard',
        name: 'Dashboard',
        component: Dashboard,
        meta: {
            title: 'Dashboard - Cartfuel Portal',
        },
    },
    {
        path: '/domain-settings',
        name: 'DomainSettings',
        component: DomainSettings,
        meta: {
            title: 'Domain Settings - Cartfuel Portal',
        },
    },
    {
        path: '/connect-stripe',
        name: 'ConnectStripe',
        component: ConnectStripe,
        meta: {
            title: 'Stripe Onboarding - Cartfuel Portal',
        },
    },
    {
        path: '/configure-stripe',
        name: 'ConfigureStripe',
        component: ConfigureStripe,
        meta: {
            title: 'Configure Stripe - Cartfuel Portal',
        },
    },
    {
        path: '/login',
        name: 'Login',
        component: Login,
        meta: {
            title: 'Login - Cartfuel Portal',
        },
    },
    {
        path: '/register',
        name: 'Register',
        component: Register,
        meta: {
            title: 'Register - Cartfuel Portal',
        },
    },
    {
        path: '/confirm-login',
        name: 'ConfirmLogin',
        component: ConfirmLogin,
        meta: {
            title: 'Confirm your Login - Cartfuel Portal',
        },
    },
    {
        path: '/check-your-email',
        name: 'CheckEmail',
        component: CheckEmail,
        meta: {
            title: 'Check your Email - Cartfuel Portal',
        },
    },
    {
        path: '/confirm-account',
        name: 'ConfirmAccount',
        component: ConfirmAccount,
        meta: {
            title: 'Confirm your Account - Cartfuel Portal',
        },
    },
    {
        path: '/access/:company',
        name: 'AccessPage',
        component: AccessPage
    },
    {
        path: '/access/:company/check-your-email',
        name: 'CustomerCheckEmail',
        component: CustomerCheckEmail
    },
    {
        path: '/access/:company/authenticate/:token',
        name: 'AccessDashboard',
        component: AccessDashboard
    },
    {
        path: '/dashboard/launch-portal/:customer_id',
        name: 'LaunchPortal',
        component: LaunchPortal,
        meta: {
            title: 'Stripe Billing - Cartfuel Portal',
        },
    },
    {
        path: '/:pathMatch(.*)*',
        name: 'PageNotFound',
        component: PageNotFound,
        meta: {
            title: 'Not Found - Cartfuel Portal',
        },
    }
]

const router = createRouter({
    history: createWebHistory(process.env.BASE_URL),
    routes,
})

router.beforeEach((toRoute, fromRoute, next) => {
    window.document.title = toRoute.meta && toRoute.meta.title ? toRoute.meta.title : 'Cartfuel Portal';
    next();
})

export default router