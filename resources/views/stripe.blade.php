
<section data-bgcolor="#fff" class="pt40 pb40 text-dark" style="margin-top:100px;">
<div class="container">

    @if ($show == 'form')
    <a href="/stripe/23456">Start</a>
    @else
    Stripe account has been connected and the account_id is {{'id'}}
    @endif

</div>
<br/><br/>


<footer class="dark">
<div class="container">
<div class="row">
<div class="col-md-6 sm-mb10">
<div class="mt10">&copy; Copyright 2021 - Centinel </div>
</div>
<div class="col-md-6 text-left text-md-right">
<div class="social-icons">
<a href="#"><i class="fa fa-facebook fa-lg"></i></a>
<a href="#"><i class="fa fa-twitter fa-lg"></i></a>
<a href="#"><i class="fa fa-linkedin fa-lg"></i></a>
<a href="#"><i class="fa fa-google-plus fa-lg"></i></a>
<a href="#"><i class="fa fa-rss fa-lg"></i></a>
</div>
</div>
</div>
</div>
</footer>

</div>



<script src="{{asset('assets/js/bootstrap.min.js')}}"></script>
<script src="{{asset('assets/js/jquery.isotope.min.js')}}"></script>
<script src="{{asset('assets/js/easing.js')}}"></script>
<script src="{{asset('assets/js/owl.carousel.js')}}"></script>
<script src="{{asset('assets/js/jquery.countTo.js')}}"></script>
<script src="{{asset('assets/js/wow.min.js')}}"></script>
<script src="{{asset('assets/js/jquery.magnific-popup.min.js')}}"></script>
<script src="{{asset('assets/js/enquire.min.js')}}"></script>
<script src="{{asset('assets/js/jquery.stellar.min.js')}}"></script>
<script src="{{asset('assets/js/jquery.plugin.js')}}"></script>
<script src="{{asset('assets/js/jquery.easeScroll.js')}}"></script>
<script src="{{asset('assets/js/designesia.js')}}"></script>
<script src="{{asset('assets/js/validation.js')}}"></script>

